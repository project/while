<?php

/**
 * @file
 * This file contains no working PHP code; it exists to provide additional
 * documentation for doxygen as well as to document hooks in the standard
 * Drupal manner.
 */

/**
 * @addtogroup hooks
 * @{
 */

/**
* Act on while label entities being loaded from the database.
*
* This hook is invoked during white label entity loading, which is handled by
* entity_load(), via the EntityCRUDController.
*
* @param $entities
*   An array of while entities being loaded, keyed by id.
*
* @see hook_entity_load()
*/
function hook_while_load($entities) {
  $result = db_query('SELECT pid, foo FROM {mytable} WHERE pid IN(:ids)', array(':ids' => array_keys($entities)));
  foreach ($result as $record) {
    $entities[$record->pid]->foo = $record->foo;
  }
}

/**
* Respond when a white label entity is inserted.
*
* This hook is invoked after the white label entity is inserted into the database.
*
* @param $while
*   The white label entity that is being inserted.
*
* @see hook_entity_insert()
*/
function hook_while_insert($while) {
  db_insert('mytable')
    ->fields(array(
      'pid' => $while->pid,
      'extra' => $while->extra,
    ))
    ->execute();
}

/**
* Act on a white label entity being inserted or updated.
*
* This hook is invoked before the white label entity is saved to the database.
*
* @param $while
*   The white label entity that is being inserted or updated.
*
* @see hook_entity_presave()
*/
function hook_while_presave($while) {
  $while->extra = 'foo';
}

/**
* Respond to a white label entity being updated.
*
* This hook is invoked after the white label entity has been updated in the database.
*
* @param $while
*   The $while that is being updated.
*
* @see hook_entity_update()
*/
function hook_while_update($while) {
  db_update('mytable')
    ->fields(array('extra' => $while->extra))
    ->condition('pid', $while->pid)
    ->execute();
}

/**
* Respond to white label entity deletion.
*
* This hook is invoked after the white label entity has been removed from the database.
*
* @param $while
*   The white label entity that is being deleted.
*
* @see hook_entity_delete()
*/
function hook_while_delete($while) {
  db_delete('mytable')
    ->condition('pid', $while->pid)
    ->execute();
}

/**
* Act on a white label entity that is being assembled before rendering.
*
* @param $while
*   The white label entity entity.
* @param $view_mode
*   The view mode the white label entity is rendered in.
* @param $langcode
*   The language code used for rendering.
*
* The module may add elements to $while->content prior to rendering. The
* structure of $while->content is a renderable array as expected by
* drupal_render().
*
* @see hook_entity_prepare_view()
* @see hook_entity_view()
*/
function hook_while_view($while, $view_mode, $langcode) {
  $while->content['my_additional_field'] = array(
    '#markup' => $additional_field,
    '#weight' => 10,
    '#theme' => 'mymodule_my_additional_field',
  );
}

/**
* Alter the results of entity_view() for while label entities.
*
* @param $build
*   A renderable array representing the white label entity content.
*
* This hook is called after the content has been assembled in a structured
* array and may be used for doing processing which requires that the complete
* white label entity content structure has been built.
*
* If the module wishes to act on the rendered HTML of the white label entity rather than
* the structured content array, it may use this hook to add a #post_render
* callback. Alternatively, it could also implement hook_preprocess_while().
* See drupal_render() and theme() documentation respectively for details.
*
* @see hook_entity_view_alter()
*/
function hook_while_view_alter($build) {
  if ($build['#view_mode'] == 'full' && isset($build['an_additional_field'])) {
    // Change its weight.
    $build['an_additional_field']['#weight'] = -10;

    // Add a #post_render callback to act on the rendered HTML of the entity.
    $build['#post_render'][] = 'my_module_post_render';
  }
}

/**
 * Act on white label entity type being loaded from the database.
 *
 * This hook is invoked during white label entity type loading, which is handled by
 * entity_load(), via the EntityCRUDController.
 *
 * @param $types
 *   An array of while label entities being loaded, keyed by white label entity type names.
 */
function hook_while_type_load($types) {
  if (isset($types['main'])) {
    $types['main']->userCategory = FALSE;
    $types['main']->userView = FALSE;
  }
}

/**
 * Respond when a white label entity type is inserted.
 *
 * This hook is invoked after the white label entity type is inserted into the database.
 *
 * @param $type
 *   The white label entity type that is being inserted.
 */
function hook_while_type_insert($type) {
  db_insert('mytable')
    ->fields(array(
      'id' => $type->id,
      'extra' => $type->extra,
    ))
    ->execute();
}

/**
 * Act on a white label entity type being inserted or updated.
 *
 * This hook is invoked before the white label entity type is saved to the
 * database.
 *
 * @param $type
 *   The white label entity type that is being inserted or updated.
 */
function hook_while_type_presave($type) {
  $type->extra = 'foo';
}

/**
 * Respond to updates to a white label entity.
 *
 * This hook is invoked after the white label entity type has been updated in
 * the database.
 *
 * @param $type
 *   The white label entity type that is being updated.
 */
function hook_while_type_update($type) {
  db_update('mytable')
    ->fields(array('extra' => $type->extra))
    ->condition('id', $type->id)
    ->execute();
}

/**
 * Respond to white label entity type deletion.
 *
 * This hook is invoked after the white label entity type has been removed from
 * the database.
 *
 * @param $type
 *   The white label entity type that is being deleted.
 */
function hook_while_type_delete($type) {
  db_delete('mytable')
    ->condition('id', $type->id)
    ->execute();
}

/**
 * Define default white label entity type configurations.
 *
 * @return
 *   An array of default white label entity types, keyed by white label entity
 *   type names.
 */
function hook_default_while_type() {
  $types['main'] = new white label entityType(array(
      'type' => 'main',
      'label' => t('white label entity'),
      'weight' => 0,
      'locked' => TRUE,
  ));
  return $types;
}

/**
* Alter default white label entity type configurations.
*
* @param $defaults
*   An array of default white label entity types, keyed by type names.
*
* @see hook_default_while_type()
*/
function hook_default_while_type_alter(&$defaults) {
  $defaults['main']->label = 'custom label';
}

/**
 * Alter while forms.
 *
 * Modules may alter the while entity form regardless to which form it is
 * attached by making use of this hook or the white label entity type specifiy
 * hook_form_while_edit_white label entity_TYPE_form_alter(). #entity_builders
 * may be used in order to copy the values of added form elements to the entity,
 * just as described by entity_form_submit_build_entity().
 *
 * @param $form
 *   Nested array of form elements that comprise the form.
 * @param $form_state
 *   A keyed array containing the current state of the form.
 *
 * @see while_attach_form()
 */
function hook_form_while_form_alter(&$form, &$form_state) {
  // Your alterations.
}

/**
 * Control access to while label entities.
 *
 * Modules may implement this hook if they want to have a say in whether or not
 * a given user has access to perform a given operation on a white label entity.
 *
 * @param $op
 *   The operation being performed. One of 'view', 'edit' (being the same as
 *   'create' or 'update') and 'delete'.
 * @param $while
 *   (optional) A white label entity to check access for. If nothing is given,
 *   access for all while label entities is determined.
 * @param $account
 *   (optional) The user to check for. If no account is passed, access is
 *   determined for the global user.
 * @return boolean
 *   Return TRUE to grant access, FALSE to explicitly deny access. Return NULL
 *   or nothing to not affect the operation.
 *   Access is granted as soon as a module grants access and no one denies
 *   access. Thus if no module explicitly grants access, access will be denied.
 *
 * @see while_access()
 */
function hook_while_access($op, $while = NULL, $account = NULL) {
  if (isset($while)) {
    // Explicitly deny access for a 'secret' white label entity type.
    if ($while->type == 'secret' && !user_access('custom permission')) {
      return FALSE;
    }
    // For while label entities other than the default white label entity grant access.
    if ($while->type != 'main' && user_access('custom permission')) {
      return TRUE;
    }
    // In other cases do not alter access.
  }
}

/**
 * @}
 */
